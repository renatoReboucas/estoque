<?php
session_start();
class Conexao
{

    public function config()
    {
        session_set_cookie_params(120000);
        session_start();
        ini_set("memory_limit", "48M");
        date_default_timezone_set('America/Sao_Paulo');
        $link = new PDO('mysql:host=127.0.0.1;port=8889;dbname=admin', 'root', 'root');
        mysqli_set_charset($link,'utf8');
        return $link;
    }

   
}