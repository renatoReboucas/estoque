<?php require_once("classes/Usuario.php"); ?>
<?php
//verifica autenticacao do usuario
$usuario = new Usuario();
$verificaUser = $usuario->verificaUser($_SESSION["id_usuario"]);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <?php include("css/css.php"); ?> 
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="css/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/node_modules/mdbootstrap/css/mdb.min.css" rel="stylesheet" />
    <link href="css/node_modules/mdbootstrap/css/style.min.css" rel="stylesheet" />
</head>

<body>

    <?php include("navbar.php"); ?>

    <div id="content">

        <?php
        if (isset($_REQUEST['page']) && preg_match("/^[a-zA-Z0-9-]+$/", $_REQUEST['page'])) {
            //echo $_REQUEST['page'];
            include("int0" . $_REQUEST['page'] . ".php");
        } else {
            include("int0dashboard.php");
        }
        ?>
        <div class="row-fluid">
            <div id="footer" class="span12">

            </div>
        </div>
    </div>

    <?php include("js/js.php"); ?>
    <!--    <script src="index.js"></script>-->
</body>

</html>